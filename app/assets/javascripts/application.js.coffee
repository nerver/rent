#= require jquery
#= require jquery_ujs
#= require jquery.selectify
#= require flats
#= require modal
#= require infinitescroll
#= require jquery.scroll_here

$ ->
  $('select').selectify()
  $('#menu-toggler').click ->
    $('#menu').toggle()

  $('.flat').flatDetails()

  $('[data-modal]').modalize()
  $('#loginmenu').modalize('login')
  $('.register').modalize('login')

  # preload images

  $.each ['/al.gif'], ->
    img = new Image()
    img.src = this

  $('.show-phone').click (e) ->
    e.stopPropagation()
    # e.preventDefault()

  $('.magick-wrapper').css 'width', $('body').innerWidth()

  $(window).resize ->
    $('.magick-wrapper').css 'width', $('body').innerWidth()

  $('.similar-flat').click ->
    window.location.href = $(this).find('a').attr('href')

$.fn.extend
  flatDetails: ->
    return @each ->
      $(this).click ->
        if $(this).hasClass('expanded')
          $("#" + $(this).attr('id') + "-details").remove()
          $(".ajax-loader").remove()
        else
          loader = $('<div class="details ajax-loader"><div class="inner"><img src="/al.gif" alt="Загрузка..." title="Загрузка..." /></div></div>')
          $(this).after(loader)
          $.ajax
            url: $(this).data 'url'
            dataType: 'script'
        $(this).toggleClass('expanded')
  sortable: ->
    sortables = []
    @each ->
      sortables.push this
    return @each ->
      $(this).click ->
        newDir = if $(this).hasClass('desc') then 'asc' else 'desc'
        $(sortables).each ->
          $(this).removeClass('asc').removeClass('desc')
        $(this).addClass newDir
        $('#dir').val newDir
        $('#sort').val($(this).data('sort'))
        $('#finder form').submit()


$ ->
  fields = $('#district, #type, #price_from, #price_to, #address')

  cleanerVisibility = ->
    showCleaner = false
    fields.each ->
      if $(this).val() != ''
        showCleaner = true
    if showCleaner
      $('#finder-cleaner').show()
    else
      $('#finder-cleaner').hide()

      
  $('.filters select').change ->
    $('#finder form').submit()
  $('#address').keyup ->
    cleanerVisibility()
    $('#finder form').submit()
  $('div[data-sort]').sortable()

  $('#finder-cleaner').click ->
    fields.each ->
      $(this).val('')
      $(this).trigger 'change'
    false


  cleanerVisibility()

  fields.change ->
    cleanerVisibility()




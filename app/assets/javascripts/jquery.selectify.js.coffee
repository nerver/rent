$.fn.selectify = ->
  this.hide()
  this.each ->
    select = this
    fake = $('<div class="selectify"></div>')
    dad = $('<div class="select">' + $(select).children('option:selected').html() + '</div>')
    fake.append dad
    ul = $('<ul class="options-wrapper"></ul>')
    ul.hide()
    $(this).after fake
    fake.append ul
    $(this).children().each ->
      value = $(this).attr 'value'
      return if value == ''
      html = $(this).html()
      id = select.id
      ul.append '<li class="option" data-selectify-value="' + value + '" data-selectify-source-id="' + id + '">' + html + '</li>'
    dad.click ->
      ul = $(this).siblings().first()
      $(".selectify .options-wrapper").each ->
        unless $(this).is $(ul)
          $(this).hide()
      ul.toggle();
      if ul.is(':visible')
        $('.selectify .select').addClass('select-active')
      else
        $('.selectify .select').removeClass('select-active')
    $(this).change ->
      $(this).siblings('.selectify').children('.select').html $(this).children(':selected').html()
  $('.selectify .option').click ->
    id = $(this).data('selectify-source-id')
    value = $(this).data('selectify-value')
    $('#' + id).val(value).change()
    ul = $(this).parent()
    ul.hide()
    ul.siblings().first().html($('#' + id).children('option:selected').html())
    $('.selectify .select').removeClass('select-active')

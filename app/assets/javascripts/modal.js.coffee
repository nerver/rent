$.fn.modalize = (target) ->
  $(this).each ->
    if target
      $(this).data('modal', target)
    $(this).click (e) ->
      $('body').css('overflow', 'hidden')
      $("##{$(this).data('modal')}").fadeIn 200
      $(document).on 'touchmove', (e) ->
        e.preventDefault()
      # $("##{$(this).data('modal')}").on 'touchstart', (e) ->
        # e.preventDefault()
      # document.ontouchstart = (e) ->
        # e.preventDefault()
      e.preventDefault()
      e.stopPropagation()
    $("##{$(this).data('modal')}").children('.modal-close').click (e) ->
      $(this).parent().fadeOut 100, ->
        $('body').css('overflow', 'auto')
      $(document).off 'touchmove'
      e.preventDefault()
      e.stopPropagation()
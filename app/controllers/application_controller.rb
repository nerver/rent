class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :current_user
  helper_method :user_signed_in?

  def after_sign_in_path_for(resource)
    if current_user.subscribed?
      request.env['omniauth.origin'] || stored_location_for(resource) || root_path
    else
      page_path('prices')
    end
  end

  private

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def user_signed_in?
    current_user.present?
  end
end

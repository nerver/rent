class FlatsController < ApplicationController
  def index
    @flats = Flat.includes(:type, :district)
      .actual
      .filter(params.slice(:address, :district, :type, :price_from, :price_to))
      .sortable(params[:sort], params[:dir])
      .page(params[:page])
      .decorate
    @districts = District.all
    @types = Type.all
    @price_from_steps = APP_CONFIG[:price_from_steps]
    @price_to_steps = APP_CONFIG[:price_to_steps]

    respond_to :html, :js
  end
  def show
    @flat = Flat.find(params[:id]).decorate

    respond_to :html, :js
  end
end

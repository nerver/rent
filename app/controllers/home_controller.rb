class HomeController < ApplicationController
  def show
    @flats = Flat.includes(:type, :district).actual.page(params[:page]).decorate
    @districts = District.all
    @types = Type.all
    @price_from_steps = APP_CONFIG[:price_from_steps]
    @price_to_steps = APP_CONFIG[:price_to_steps]
  end
end

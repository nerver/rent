class OrdersController < ApplicationController
  def create
    @order = Order.create user: current_user, amount: params[:amount], is_done: false
    @liqpay_request = Liqpay::Request.new(
      amount: @order.amount,
      order_id: @order.id,
      description: 'Оплата подписки на сайте Sherlock Homes',
      result_url: orders_url
    )
  end
  def index
  end
end

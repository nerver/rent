class PaymentsController < ApplicationController
  protect_from_forgery except: :liqpay_payment
  skip_before_filter :verify_authenticity_token
  def create
    @liqpay_response = Liqpay::Response.new(params)
    logger.info "Order ID: #{@liqpay_response.order_id.to_i}"
    logger.info "Amount: #{@liqpay_response.amount.to_i}"
    if @liqpay_response.success?
      order = Order.where(
        id: @liqpay_response.order_id.to_i, 
        amount: @liqpay_response.amount.to_i,
        is_done: false
      ).first
      order.try(:accept)
      logger.info "Order: #{order.id}"
    end
  render plain: 'OK'
  rescue Liqpay::InvalidResponse
  end
end

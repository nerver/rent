class FlatDecorator < Draper::Decorator
  delegate_all

  def static_map
    h.image_tag "http://static-maps.yandex.ru/1.x/?ll=#{pos_x},#{pos_y}&spn=0.002,0.002&size=600,450&l=map" +
      "&pt=#{pos_x},#{pos_y},pm2rdm", id: "flat-#{id}-map"
  end

  def actual_at_date
    Russian::strftime actual_at, "%d %b" 
  end

  def contact
    if h.user_signed_in?
      if h.current_user.subscribed?
        phone
      else
        h.link_to "Показать телефон", h.page_path(:prices), class: 'show-phone button'
      end
    else
      h.link_to "Показать телефон", "#", data: {modal: 'login'}, class: 'show-phone button'
    end
  end

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

end

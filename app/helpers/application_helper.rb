module ApplicationHelper
  def full_url path
    path.nil? ? nil : request.protocol + request.host_with_port + path
  end
end

module Presentable
  extend ActiveSupport::Concern

  def shown_name
    presentation_name || name
  end
end
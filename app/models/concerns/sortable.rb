module Sortable
  extend ActiveSupport::Concern

  module ClassMethods
    def sortable(sorting_column, direction = 'desk')
      result = self.where(nil)
      return result unless self.column_names.include?(sorting_column)
      result.reorder "#{sorting_column} #{direction == 'desc' ? 'DESC' : 'ASC'}"
    end
  end
end
class District < ActiveRecord::Base
  include Presentable
  has_many :flats
end

class Flat < ActiveRecord::Base

  mount_uploader :og_preview, OgPreviewUploader

  include Filterable
  include Sortable

  default_scope -> {order "actual_at DESC"}

  # Filters
  scope :price_from,  -> (price_from) { where "price >= ?", price_from }
  scope :price_to,    -> (price_to)   { where "price <= ?", price_to }
  scope :district,    -> (district)   { where district_id: district }
  scope :address,     -> (address)    { where "address LIKE ?", "%#{address}%" }
  scope :type,        -> (type)       { where type_id: type }

  scope :actual, -> { where "actual_at >= ?", 1.week.ago.utc }

  before_create :update_datestamp
  before_save :generate_og_preview

  belongs_to :district
  belongs_to :type

  def update_datestamp
    actual_at = Date.today
  end

  def similars count = 4
    Flat.actual.where('price < 20000').order('RAND()').limit count
  end

  protected

  def generate_og_preview
    img = MiniMagick::Image.open(Dir[Rails.root.join('app', 'assets', 'images', 'patterns', '*')].sample)
    wrapped_address = Helper.word_wrap(address, line_width: 25)
    wrapped_district = Helper.word_wrap(district.name, line_width: 25)
    caption = "#{wrapped_address}\n#{wrapped_district}\n#{price} руб/мес"
    img.combine_options do |c|
      c.font Rails.root.join('app', 'assets', 'fonts', 'open-sans.ttf')
      c.gravity 'Center'
      c.pointsize '21'
      c.draw "text 0, -10 '#{caption}'"
      c.fill 'white'
      c.gravity 'South'
      c.pointsize '12'
      c.draw "text 0,10 'Ищите больше на sherlock-homes.ru'"
    end
    self.og_preview = img
  end

  module Helper
    extend ActionView::Helpers::TextHelper
  end
end

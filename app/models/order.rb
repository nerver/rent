class Order < ActiveRecord::Base
  belongs_to :user

  def accept
    amount_days = APP_CONFIG[:plans][self.amount.to_i]
    self.user.extend_subscription! amount_days
    self.is_done = true
    self.save
  end
end

class Type < ActiveRecord::Base
  include Presentable
  has_many :flats
end

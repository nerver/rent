class User < ActiveRecord::Base

  has_many :orders

  before_create :set_subscription_expire

  def self.create_with_omniauth(auth)
    create! do |user|
      user.provider = auth["provider"]
      user.uid = auth["uid"]
    end
  end

  def subscribed?
    self.subscription_expire_at > DateTime.now
  end

  def subscription_days_left
    ((self.subscription_expire_at - DateTime.now) / 1.day).to_i + 1
  end

  def extend_subscription! amount
    if self.subscribed?
      self.subscription_expire_at += amount.days
    else
      self.subscription_expire_at = DateTime.now + amount.days
    end  
    self.save
  end

  protected

  def set_subscription_expire
    self.subscription_expire_at = DateTime.now
  end
end

# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'rent'
set :repo_url, 'git@bitbucket.org:nerver/rent.git'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

# Default deploy_to directory is /var/www/my_app
set :deploy_to, '/u/apps/rent_production'

# Use agent forwarding for SSH so you can deploy with the SSH key on your workstation.
set :ssh_options, {
  forward_agent: true
}

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
set :linked_files, %w{config/database.yml .rbenv-vars}

# Default value for linked_dirs is []
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/uploads}

# Default value for default_env is {}
set :default_env, { path: "/opt/rbenv/shims:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  namespace :rentgrabber do

    desc "Run rent scheduler"
    task :start do
      on roles(:app) do
        within "#{current_path}" do
          with rails_env: :production do
            rake "daemon:rentgrabber:start"
          end
        end
      end
    end

    desc "Stop rent scheduler"
    task :stop do
      on roles(:app) do
        within "#{current_path}" do
          with rails_env: :production do
            rake "daemon:rentgrabber:stop"
          end
        end
      end
    end

  end

  after :publishing, :restart

end
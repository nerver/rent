Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook, APP_CONFIG[:omniauth][:facebook][:key], APP_CONFIG[:omniauth][:facebook][:secret]
  provider :vkontakte, APP_CONFIG[:omniauth][:vkontakte][:key], APP_CONFIG[:omniauth][:vkontakte][:secret]
  provider :twitter, APP_CONFIG[:omniauth][:twitter][:key], APP_CONFIG[:omniauth][:twitter][:secret]
end
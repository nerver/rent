SimpleNavigation::Configuration.run do |navigation|
  navigation.items do |primary|
    primary.dom_id = 'menu'
    primary.item :how_to_pay, 'Как оплатить', '#'
    primary.item :divider_1, '/', class: 'divider'
    primary.item :prices, 'Цены', page_path('prices')
    primary.item :divider_2, '/', class: 'divider'
    primary.item :about, 'О нас', page_path('about')
    primary.item :divider_3, '/', class: 'divider'
    primary.item :about_us, 'Контакты', page_path('contacts')
    primary.item :divider_4, '/', class: 'divider'
    primary.item :loginmenu, 'Вход', '#', unless: proc { user_signed_in? }
    primary.item :logoutmenu, 'Выход', signout_path, method: :delete, if: proc { user_signed_in? }
    primary.item :divider_5, '/', class: 'divider', if: proc { user_signed_in? }
    primary.item :divider_5, "Подписка: #{current_user.try(:subscription_days_left)} дн.", class: 'nav-info', if: proc { user_signed_in? and current_user.subscribed? }
    primary.item :prices, 'Получить  доступ', page_path('prices'), if: proc { user_signed_in? and !current_user.subscribed? }
  end
end

Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/cp', as: 'rails_admin'
  resources :flats, only: [:show, :index]

  resources :payments, only: [:create]

  resources :orders, only: [:create, :index]

  # oauth

  get "/auth/:provider/callback" => "sessions#create"
  delete "/signout" => "sessions#destroy", as: :signout

  root to: 'home#show'
end

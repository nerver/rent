class CreateFlats < ActiveRecord::Migration
  def change
    create_table :flats do |t|
      t.string :address, null: false
      t.string :phone, null: false
      t.integer :price
      t.integer :district_id, null: false
      t.integer :type_id, null: false
      t.text :description
      t.float :pos_x
      t.float :pos_y
      t.date :actual_at
      t.boolean :active, default: true

      t.timestamps
    end
    add_index :flats, :active
    add_index :flats, :district_id
    add_index :flats, :type_id
  end
end

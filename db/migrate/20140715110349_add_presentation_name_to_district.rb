class AddPresentationNameToDistrict < ActiveRecord::Migration
  def change
    add_column :districts, :presentation_name, :string
  end
end

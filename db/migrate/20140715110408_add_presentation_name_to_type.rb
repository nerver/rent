class AddPresentationNameToType < ActiveRecord::Migration
  def change
    add_column :types, :presentation_name, :string
  end
end

class AddSubscriptionExpireAtToUser < ActiveRecord::Migration
  def change
    add_column :users, :subscription_expire_at, :datetime
  end
end

class AddOgPreviewToFlat < ActiveRecord::Migration
  def change
    add_column :flats, :og_preview, :string
  end
end

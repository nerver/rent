class AddIsPromoToFlat < ActiveRecord::Migration
  def change
    add_column :flats, :is_promo, :boolean, default: false
  end
end

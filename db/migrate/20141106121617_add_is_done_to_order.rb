class AddIsDoneToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :is_done, :boolean, default: false
  end
end

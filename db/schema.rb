# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141106121617) do

  create_table "districts", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "presentation_name"
  end

  create_table "flats", force: true do |t|
    t.string   "address",                                null: false
    t.string   "phone",                                  null: false
    t.float    "price",       limit: 24
    t.integer  "district_id",                            null: false
    t.integer  "type_id",                                null: false
    t.text     "description"
    t.float    "pos_x",       limit: 24
    t.float    "pos_y",       limit: 24
    t.date     "actual_at"
    t.boolean  "active",                 default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "og_preview"
    t.boolean  "is_promo",               default: false
  end

  add_index "flats", ["active", "district_id", "type_id"], name: "index_flats_on_active_and_district_id_and_type_id", using: :btree
  add_index "flats", ["active"], name: "index_flats_on_active", using: :btree
  add_index "flats", ["district_id"], name: "index_flats_on_district_id", using: :btree
  add_index "flats", ["type_id"], name: "index_flats_on_type_id", using: :btree

  create_table "orders", force: true do |t|
    t.integer  "amount"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_done",    default: false
  end

  create_table "types", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "presentation_name"
  end

  create_table "users", force: true do |t|
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "subscription_expire_at"
  end

end

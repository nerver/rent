#!/usr/bin/env ruby

# You might want to change this
ENV["RAILS_ENV"] ||= "production"

root = File.expand_path(File.dirname(__FILE__))
root = File.dirname(root) until File.exists?(File.join(root, 'config'))
Dir.chdir(root)

require File.join(root, "config", "environment")

# $running = true
# Signal.trap("TERM") do 
  # $running = false
# end

require Rails.root.join 'lib', 'rentgrabber', 'rentgrabber'

grabber = RentGrabber.new APP_CONFIG[:bots]

# while($running) do

  # scheduler = Rufus::Scheduler.singleton

  # scheduler.every '2h', first: :now do
    grabber.run

    # sleep 7200
  # end

  # scheduler.join
# end

class Bot
  attr_reader :results
  attr_reader :log

  @@subclasses = { }
  def initialize bot_id
    @bot_id = bot_id
    @agent = Mechanize.new
    @agent.user_agent_alias = 'Windows Mozilla'
    @log = RentGrabber.log
    log.debug "New bot created"
  end
  def self.create bot_id
    (@@subclasses[bot_id] || raise("Bad bot id: #{bot_id}")).new bot_id
  end
  def self.register_bot bot_id
    @@subclasses[bot_id] = self
  end

  def authorize username, password
    @username, @password = username, password
    restore_cookies
    login unless logged?
    raise "Can't login" unless logged?
    store_cookies
  end

  def grab!
    raise "Not logged in" unless logged?
    grabber_logic
  end

  protected

  def grabber_logic
    raise "Override"
  end

  def logged?
    raise "Override!"
  end

  def login
    raise "Override!"
  end

  private

  def store_cookies
    @agent.cookie_jar.save_as cookie_filename
  end

  def restore_cookies
    @agent.cookie_jar.load cookie_filename if File.exist? cookie_filename
  end

  def cookie_filename
    Rails.root.join("tmp", "cookies", "#{@bot_id}_cookie.yml").to_s
  end
end
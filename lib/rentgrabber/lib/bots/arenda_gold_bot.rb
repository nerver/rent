class ArendaGoldBot < Bot
  register_bot :arenda_gold

  def login
    @agent.get 'http://mail.arenda-gold.ru/' do |page|
      page.encoding = "utf-8"
      form = page.forms.first 
      form.login = @username
      form.password = @password
      form.submit
    end 
  end

  def logged?
    !@agent.get('http://mail.arenda-gold.ru/?').search('table.messages').empty?
  end

  def grabber_logic
    @results = [] 
    (0..5).each do |str|
      @agent.get "http://mail.arenda-gold.ru/?str=#{str}" do |page|

        page.encoding = "utf-8"

        trs = page.search('.messages > tr')

        trs.shift

        while tr = trs.shift do
          tds = tr.search('td')
          res = {}
          res[:inner_id]    = tds[0].search('input').first['value']
          res[:date]        = Date.today
          res[:address]     = tds[3].text.strip
          res[:price]       = tds[4].text.strip
          res[:phone]       = tds[6].text.strip
          res[:destrict]    = (res[:address].split(',')[1].strip rescue '')
          res[:type]        = case tds[2].text.strip
            when 'Помесячно сдам 1-комнатную квартиру' then 1
            when 'Помесячно сдам 2-комнатную квартиру' then 2
            when 'Помесячно сдам 3-комнатную квартиру' then 3
            when 'Помесячно сдам 4-комнатную квартиру' then 4
            when 'Помесячно сдам комнату' then 0
            else 
              -1 
            end

          tdd = trs.shift

          res[:description] = Sanitize.fragment(tdd.to_s, elements: ['br'])

          @results << res
        end 
      end
    end
  end
end
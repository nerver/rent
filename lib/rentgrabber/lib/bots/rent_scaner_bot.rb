class RentScanerBot < Bot
  def login
    @agent.get 'http://rent-scaner.ru/' do |page|
      page.form_with(id: 'login-form') do |form|
        form.username = @username
        form.password = @password
        form.checkbox_with(name: 'remember').check
      end.submit
    end 
  end

  def logged?
    !@agent.get('http://rent-scaner.ru/one-tape?site=rent').form_with(name: 'filter').nil?
  end

  def grabber_logic
    @results = [] 
    @agent.get 'http://rent-scaner.ru/one-tape?site=rent' do |page|
      page.search('.tr_offer').each do |offer|
        rooms = offer.search('.room b').first.text.strip
        res = {}
        res[:inner_id]    = offer[:id].gsub('offer_', '')
        res[:description] = offer.search('.description').first.text.strip
        res[:date]        = Date.today
        res[:destrict]    = ''
        res[:type]        = (rooms == '-') ? 0 : rooms.to_i
        res[:address]     = offer.search('.address').first.text.strip 
        res[:phone]       = offer.search('.phone').first.text.strip
        res[:price]       = offer.search('.price').first.text.strip

        @results << res
      end
    end
  end

  register_bot :rent_scaner
end
require 'json'

class VoshodNskBot < Bot
  register_bot :voshod_nsk

  def login
    resp = @agent.post 'http://api.voshod-nsk.ru/login/identify', {username: @username, password: @password}.to_json
    json = JSON.parse(resp.body)
    id = json['response'].first['id']

    resp = @agent.post 'http://api.voshod-nsk.ru/login/signin', {realtor_id: 294}.to_json
  end

  def logged?
    resp = @agent.get('http://api.voshod-nsk.ru/vad/search?type=3&street=&house_num=&from_price=&to_price=&pages=1&offset=0').body
    json = JSON.parse(resp)
    not (json['status'] && json['status'] == 'error')
  end

  def grabber_logic
    @agent.get 'http://api.voshod-nsk.ru/vad/search?type=3&street=&house_num=&from_price=&to_price=&pages=1&offset=0' do |page|
      json = JSON.parse(page.body)

      @results = []

      json['ads'].each do |ad|
        res = {}
        res[:inner_id]    = ad['id']
        res[:date]        = Date.today
        res[:address]     = ad['address']
        res[:price]       = ad['price']
        res[:phone]       = ad['contact_phone']
        res[:destrict]    = ''
        res[:type]        = ad['rooms_num']
        res[:description] = ad['additional_comment']
        @results << res
      end
    end
  end
end
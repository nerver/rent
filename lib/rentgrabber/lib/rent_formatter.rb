class RentFormatter
  attr_reader :actual_at
  attr_reader :price
  attr_reader :type
  attr_reader :district
  attr_reader :address
  attr_reader :phone
  attr_reader :description
  attr_reader :inner_id
  attr_reader :pos

  def initialize data
    @data = data

    @actual_at    = DateTime.now
    @price        = (price = data[:price].to_i) < 1000 ? price * 1000 : price
    @type         = format_type
    @phone        = format_phone
    @description  = format_description
    @inner_id     = data[:inner_id]
    
    @address, @district, @pos = geodata
  end

  def format_phone
    RentGrabber.log.debug "Formattig phone: #{@data[:phone]}"
    phoner = Phoner::Phone.parse @data[:phone].gsub /^8/, "+7"
    raise "Invalid phone: #{@data.phone}" if phoner.nil?
    phoner.format "+%c (%a) %f-%l"
  end
      
  def format_description
    desc = @data[:description]
    desc.gsub! 'URL:', ''
    desc.gsub! URI.regexp, ''
    desc.gsub! /(\+7|8)\s?\(?\d{3}\)?\s?\d{3}[-\s]?\d{2}[-\s]?\d{2}/, ''
    desc.gsub! "Телефоны:", ''
    desc.gsub! "E-mail:", ''
    desc.gsub! /.+\@.+\..+/, ''
    desc
  end

  def format_type
    case @data[:type]
    when 1 then "1-комнатная"
    when 2 then "2-комнатная"
    when 3 then "3-комнатная"
    when 4 then "4-комнатная"
    when 0 then "Комната"
    else 
      "Другое"
    end
  end

  def geodata
    return [nil, nil, nil] if @data[:address].length < 4
    a = @data[:address]
    begin
      direct = JSON.parse(RestClient.get(URI.escape("http://geocode-maps.yandex.ru/1.x/?geocode=Новосибирск #{a}&format=json")))
      pos = direct['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'].gsub(' ', ',')
      formatted = direct['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['text']
      reverse = JSON.parse(RestClient.get(URI.escape("http://geocode-maps.yandex.ru/1.x/?geocode=#{pos}&kind=district&format=json")))
      district = reverse['response']['GeoObjectCollection']['featureMember'].last['GeoObject']['name']
      return [formatted.gsub('Россия, ', ''), district, pos.split(',')]
    rescue
      return [nil, nil, nil]
    end
  end
end
require_relative 'boot'

class RentGrabber
  def initialize config
    @config = config
    RentGrabber.log.info 'Rent grabber initialized'
  end
  def self.log
    @@log ||= Logger.new(Rails.root.join('log', 'parser.log').to_s, 'monthly')
  end
  def run
    @config.each do |conf|
      RentGrabber.log.debug "Bot initialized: #{conf}"
      bot_id = conf[0].to_sym
      begin
        worker = Bot.create bot_id
        worker.authorize conf[1]['username'], conf[1]['password']
        worker.grab!
      rescue Exception => e
        RentGrabber.log.error e.message    
        next
      end
      worker.results.each do |res|
        begin
          RentGrabber.log.debug "Bot #{bot_id} got some results"
          data = RentFormatter.new res

          if data.address.nil?
            RentGrabber.log.info 'Got nil address'
            next
          end

          district = District.where(name: data.district).first

          next if district.nil?

          type = if data.price < 10000 and (data.type != "1-комнатная" or data.type != "Комната") then
            Type.find_or_create_by name: "Комната"
          else
            Type.find_or_create_by name: data.type
          end

          if Flat.where("phone = ? AND actual_at > ?", data.phone, 2.week.ago.to_date).any?
            RentGrabber.log.debug "Fresh entry with phone #{data.phone} already exists! Skipping..."
            next
          end


          Flat.create(
            address:      data.address,
            phone:        data.phone,
            price:        data.price,
            district:     district,
            type:         type,
            description:  data.description,
            pos_x:        data.pos.first,
            pos_y:        data.pos.last,
            actual_at:    data.actual_at
          )

          RentGrabber.log.debug "Entry created for phone #{data.phone}"
        rescue Exception => e
          RentGrabber.log.error e.message    
        end
      end
    end
  end
end

namespace :grabber do
  desc "Run grabber worker"
  task :run => [:environment] do
    require Rails.root.join 'lib', 'rentgrabber', 'rentgrabber'  
    grabber = RentGrabber.new APP_CONFIG[:bots]
    grabber.run
  end
end
